import React from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import globalStyles from '../styles/globalStyles';

const Settings = ({navigation}) => {
  return (
    <ImageBackground
      source={require('../assets/image/bg.png')}
      style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <View style={[styles.headerTop, globalStyles.boxShadow]}>
          <Text
            style={[
              globalStyles.headerText,
              globalStyles.fw600,
              globalStyles.f22,
              {color: '#1fc420'},
            ]}>
            EVERYDAY ISLAM
          </Text>
        </View>
        <ScrollView>
          <View
            style={[
              globalStyles.paddingHorizontal5,
              globalStyles.paddingVertical5,

              {backgroundColor: 'rgba(0, 0, 0, 0.5)'},
              {height: '100%'},
              {paddingBottom: '100%'},
            ]}>
            <TouchableOpacity
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.paddingBottom3,
              ]}
              onPress={() => navigation.navigate('PrayerTime')}>
              <View
                style={[
                  styles.circleIconArea,
                  globalStyles.boxShadow,
                  globalStyles.elevation3,
                ]}>
                <Image
                  source={require('../assets/image/ic_duas.png')}
                  style={{width: 45, height: 45}}
                />
              </View>
              <Text
                style={[
                  globalStyles.f18,
                  globalStyles.paddingTop1,
                  globalStyles.paddingLeft5,
                  {color: '#fff'},
                ]}>
                Prayer Time
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.paddingBottom3,
              ]}
              onPress={() => navigation.navigate('AzanTime')}>
              <View
                style={[
                  styles.circleIconArea,
                  globalStyles.boxShadow,
                  globalStyles.elevation3,
                ]}>
                <Image
                  source={require('../assets/image/ic_prayertime.png')}
                  style={{width: 45, height: 45}}
                />
              </View>
              <Text
                style={[
                  globalStyles.f18,
                  globalStyles.paddingTop1,
                  globalStyles.paddingLeft5,
                  {color: '#fff'},
                ]}>
                Azan
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.paddingBottom3,
              ]}
              onPress={() => navigation.navigate('Quran')}>
              <View
                style={[
                  styles.circleIconArea,
                  globalStyles.boxShadow,
                  globalStyles.elevation3,
                ]}>
                <Image
                  source={require('../assets/image/ic_qurans.png')}
                  style={{width: 45, height: 45}}
                />
              </View>
              <Text
                style={[
                  globalStyles.f18,
                  globalStyles.paddingTop1,
                  globalStyles.paddingLeft5,
                  {color: '#fff'},
                ]}>
                Quran
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.paddingBottom3,
              ]}
              onPress={() => navigation.navigate('Hadith')}>
              <View
                style={[
                  styles.circleIconArea,
                  globalStyles.boxShadow,
                  globalStyles.elevation3,
                  {color: '#fff'},
                ]}>
                <Image
                  source={require('../assets/image/ic_books.png')}
                  style={{width: 45, height: 45}}
                />
              </View>
              <Text
                style={[
                  globalStyles.f18,
                  globalStyles.paddingTop1,
                  globalStyles.paddingLeft5,
                  {color: '#fff'},
                ]}>
                Hadith
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.paddingBottom3,
              ]}
              onPress={() => navigation.navigate('Lectures')}>
              <View
                style={[
                  styles.circleIconArea,
                  globalStyles.boxShadow,
                  globalStyles.elevation3,
                ]}>
                <Image
                  source={require('../assets/image/ic_video.png')}
                  style={{width: 45, height: 45}}
                />
              </View>
              <Text
                style={[
                  globalStyles.f18,
                  globalStyles.paddingTop1,
                  globalStyles.paddingLeft5,
                  {color: '#fff'},
                ]}>
                Lectures
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.paddingBottom3,
              ]}
              onPress={() => navigation.navigate('Donation')}>
              <View
                style={[
                  styles.circleIconArea,
                  globalStyles.boxShadow,
                  globalStyles.elevation3,
                ]}>
                <Image
                  source={require('../assets/image/ic_donate.png')}
                  style={{width: 45, height: 45}}
                />
              </View>
              <Text
                style={[
                  globalStyles.f18,
                  globalStyles.paddingTop1,
                  globalStyles.paddingLeft5,
                  {color: '#fff'},
                ]}>
                Donation
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#000',
  },
  headerTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('2%'),
  },
  circleIconArea: {
    backgroundColor: '#fff',
    width: 40,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
});

export default Settings;
