import React, {useContext} from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {GlobalContext} from '../context/GlobalContext';

const AzanTime = () => {
  const {globalState, setGlobalState} = useContext(GlobalContext);

  const {timing} = globalState;
  let prayerTimes = timing;

  // Get the current date
  const currentDate = new Date();
  const currentDateString = currentDate.toDateString();

  // Filter out the prayer times that don't match the current date
  const filteredPrayerTimes = prayerTimes?.filter(prayerTime => {
    const prayerTimeDate = new Date(prayerTime.date);
    const prayerTimeDateString = prayerTimeDate.toDateString();
    return prayerTimeDateString === currentDateString;
  });

  return (
    <ImageBackground
      source={require('../assets/image/bg.png')}
      style={{flex: 1, height: '100%'}}>
      {filteredPrayerTimes?.map(prayerTime => (
        <View
          style={[styles.container, {backgroundColor: 'rgba(0, 0, 0, 0.5)'}]}
          key={prayerTime.id}>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: 20,
              marginBottom: 10,
              marginTop: 10,
            }}>
            {currentDateString}
          </Text>
          <View>
            <View style={styles.tableRow}>
              <Text style={[styles.tableCell, styles.prayerTitle]}>Fajr</Text>

              <Text style={[styles.tableCell, styles.prayerSalat]}>
                {prayerTime.fajrAdhan}
              </Text>
            </View>

            <View style={styles.tableRow}>
              <Text style={[styles.tableCell, styles.prayerTitle]}>Dhuhr</Text>

              <Text style={[styles.tableCell, styles.prayerSalat]}>
                {prayerTime.dhuhrAdhan}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={[styles.tableCell, styles.prayerTitle]}>Asr</Text>

              <Text style={[styles.tableCell, styles.prayerSalat]}>
                {prayerTime.asarAdhan}
              </Text>
            </View>

            <View style={styles.tableRow}>
              <Text style={[styles.tableCell, styles.prayerTitle]}>
                Maghrib
              </Text>

              <Text style={[styles.tableCell, styles.prayerSalat]}>
                {prayerTime.magribAdhan}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={[styles.tableCell, styles.prayerTitle]}>Isha</Text>

              <Text style={[styles.tableCell, styles.prayerSalat]}>
                {prayerTime.ishaAdhan}
              </Text>
            </View>
          </View>
        </View>
      ))}
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 4,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  table: {
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 5,
    marginBottom: 10,
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    borderTopWidth: 1,
    borderTopColor: '#ddd',
    borderLeftWidth: 1,
    borderLeftColor: '#ddd',
    borderRightWidth: 1,
    borderRightColor: '#ddd',
    margin: 5,
    backgroundColor: '#808080',
  },

  headerRow: {
    backgroundColor: '#f5f5f5',
  },
  tableCell: {
    flex: 1,
    padding: 10,
    textAlign: 'center',
  },
  tableCell2: {
    flex: 1,
    padding: 10,
    textAlign: 'center',
    width: '30%',
  },
  headerCell: {
    fontWeight: 'bold',
  },
  prayerTitle: {
    fontWeight: 'bold',
    textAlign: 'left',
    paddingLeft: 10,
    fontSize: 18,
    color: '#fff',
  },
  prayerSalat: {
    fontWeight: 'bold',
    fontSize: 18,
    // textAlign: 'left',
    // paddingLeft: 10,
    color: '#fff',
  },
  prayerTitle2: {
    fontWeight: 'bold',
    textAlign: 'left',
    paddingLeft: 10,
  },
});

export default AzanTime;
