import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  ImageBackground,
  StyleSheet,
  KeyboardAvoidingView,
  ActivityIndicator,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import stripe from 'stripe-client';
import {showToastWithGravityAndOffset} from '../utilities/components/ToastMessage';
import {Picker} from '@react-native-picker/picker';

const Donation = () => {
  const [cardNumber, setCardNumber] = useState('');
  const [expDate, setExpDate] = useState('');
  const [cvc, setCvc] = useState('');
  const [donationAmount, setDonationAmount] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [selectedMonth, setSelectedMonth] = useState('');
  const [selectedYear, setSelectedYear] = useState('');
  const [selectedDate, setSelectedDate] = useState('');

  useEffect(() => {
    setSelectedDate(`${selectedMonth}/${selectedYear}`);
  }, [selectedMonth, selectedYear]);

  const handleMonthYearChange = (value, type) => {
    if (type === 'month') {
      setSelectedMonth(value);
    } else {
      setSelectedYear(value);
    }
  };

  const months = [
    {label: '01', value: '01'},
    {label: '02', value: '02'},
    {label: '03', value: '03'},
    {label: '04', value: '04'},
    {label: '05', value: '05'},
    {label: '06', value: '06'},
    {label: '07', value: '07'},
    {label: '08', value: '08'},
    {label: '09', value: '09'},
    {label: '10', value: '10'},
    {label: '11', value: '11'},
    {label: '12', value: '12'},
  ];

  const years = [
    {label: '2022', value: '2022'},
    {label: '2023', value: '2023'},
    {label: '2024', value: '2024'},
    {label: '2025', value: '2025'},
    {label: '2026', value: '2026'},
    {label: '2027', value: '2027'},
    {label: '2028', value: '2028'},
    {label: '2029', value: '2029'},
    {label: '2030', value: '2030'},
  ];

  const handlePayPress = async () => {
    if (!donationAmount) {
      showToastWithGravityAndOffset('Please provide amount!');
    } else if (!cardNumber) {
      showToastWithGravityAndOffset('Please provide card number!');
    } else if (!cvc) {
      showToastWithGravityAndOffset('Please provide CVC!');
    } else {
      setIsLoading(true);
      try {
        const stripeClient = stripe(
          'pk_live_51IkyBxG1gtFMN7kE9w9TAx19EgPjPGWME91VVB6nK4iQF8lbUZkfqlrQqHwTfeOlhAJyUC64AHc83wKAMina14fu00AsKXIcfK',
        );

        const [expMonth, expYear] = expDate.split('/');

        const cardDetails = {
          number: cardNumber,
          exp_month: selectedMonth,
          exp_year: selectedYear,
          cvc: cvc,
        };

        const tokenResponse = await stripeClient.createToken(
          'card',
          cardDetails,
        );

        // Open a checkout session using the token returned by Stripe
        const checkoutSession = await fetch(
          'https://api.stripe.com/v1/checkout/sessions',
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization:
                'Bearer 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', // Replace with your Stripe API key
            },
            body: JSON.stringify({
              payment_method_types: ['card'],
              line_items: [
                {
                  price_data: {
                    currency: 'gbp', // set currency to gbp
                    product_data: {
                      name: 'Donation Amount',
                    },
                    unit_amount: parseFloat(donationAmount), // convert to pence
                  },
                  quantity: 1,
                },
              ],
              mode: 'payment',
              success_url: 'https://example.com/success',
              cancel_url: 'https://example.com/cancel',
            }),
          },
        );

        // Get the checkout session ID from the response
        const {id} = await checkoutSession.json();

        // Redirect the user to the checkout page
        const result = await stripeClient.redirectToCheckout({sessionId: id});

        if (result.error) {
          showToastWithGravityAndOffset('Payment Failed!');
        } else {
          showToastWithGravityAndOffset('Payment successfull!');
        }
      } catch (error) {
        showToastWithGravityAndOffset(`Payment Failed`);
      }
      setIsLoading(false);
    }
  };

  // useEffect(() => {}, [selectedDate]);

  return (
    <KeyboardAvoidingView style={styles.container1} behavior="height">
      <ImageBackground
        source={require('../assets/image/bg.png')}
        style={{flex: 1, height: '100%'}}>
        <View
          style={[styles.container, {backgroundColor: 'rgba(0, 0, 0, 0.5)'}]}>
          <View>
            <Text
              style={{
                fontSize: 18,
                color: '#1fc420',
                padding: 10,
              }}>
              Donation Amount
            </Text>
            <TextInput
              style={{
                fontSize: 18,
                color: '#fff',
                padding: 10,
                borderColor: '#fff',
                borderWidth: 1,
                marginTop: 5,
                marginLeft: 10,
                marginRight: 10,
              }}
              value={donationAmount}
              onChangeText={text => setDonationAmount(text)}
              placeholder="Donation Amount (£)"
              placeholderTextColor={'#fff'}
              keyboardType="numeric"
            />
            <Text
              style={{
                fontSize: 18,
                color: '#1fc420',
                padding: 10,
              }}>
              Card Number
            </Text>
            <TextInput
              style={{
                fontSize: 18,
                color: '#fff',
                padding: 10,
                borderColor: '#fff',
                borderWidth: 1,
                marginTop: 5,
                marginLeft: 10,
                marginRight: 10,
              }}
              placeholder="Card number"
              placeholderTextColor={'#fff'}
              keyboardType="numeric"
              value={cardNumber}
              onChangeText={setCardNumber}
            />
            <Text
              style={{
                fontSize: 18,
                color: '#1fc420',
                padding: 10,
              }}>
              MM/YY
            </Text>
            <View style={styles.inputContainer}>
              <Text style={styles.inputPicker}>{selectedDate}</Text>
              <Picker
                selectedValue={selectedMonth}
                onValueChange={value => handleMonthYearChange(value, 'month')}
                style={styles.picker}
                dropdownIconColor="#ffffff">
                <Picker.Item label="Month" value="" style={{color: '#fff'}} />
                {months.map(month => (
                  <Picker.Item
                    key={month.label}
                    label={month.label}
                    value={month.value}
                  />
                ))}
              </Picker>
              <Picker
                selectedValue={selectedYear}
                onValueChange={value => handleMonthYearChange(value, 'year')}
                style={styles.picker}
                dropdownIconColor="#ffffff">
                <Picker.Item label="Year" value="" />
                {years.map(year => (
                  <Picker.Item
                    key={year.label}
                    label={year.label}
                    value={year.value}
                  />
                ))}
              </Picker>
            </View>
            <Text
              style={{
                fontSize: 18,
                color: '#1fc420',
                padding: 10,
              }}>
              CVC
            </Text>
            <TextInput
              style={{
                fontSize: 18,
                color: '#fff',
                padding: 10,
                borderColor: '#fff',
                borderWidth: 1,
                marginTop: 5,
                marginLeft: 10,
                marginRight: 10,
              }}
              placeholder="CVC"
              placeholderTextColor={'#fff'}
              value={cvc}
              onChangeText={setCvc}
              keyboardType="numeric"
            />
            <TouchableOpacity
              style={{
                fontSize: 18,
                color: '#fff',
                backgroundColor: '#1fc420',
                padding: 10,
                borderColor: '#fff',
                borderWidth: 1,
                marginTop: 20,
                marginLeft: 10,
                marginRight: 10,
              }}
              onPress={handlePayPress}>
              {isLoading ? (
                <ActivityIndicator size="small" color="#ffffff" />
              ) : (
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#fff',
                    padding: 10,
                    textAlign: 'center',
                  }}>
                  {' '}
                  Pay Now
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#fff',
    overflow: 'hidden',
    margin: 10,
    // marginTop: 20,
  },
  inputPicker: {
    // flex: 1,
    fontSize: 18,
    color: '#fff',
    padding: 10,
  },
  picker: {
    width: 120,
    color: '#ffffff',
  },

  container1: {
    flex: 1,
    // padding: 20,
    // backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    padding: 4,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  table: {
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 5,
    marginBottom: 10,
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    borderTopWidth: 1,
    borderTopColor: '#ddd',
    borderLeftWidth: 1,
    borderLeftColor: '#ddd',
    borderRightWidth: 1,
    borderRightColor: '#ddd',
    margin: 5,
    backgroundColor: '#808080',
  },

  headerRow: {
    backgroundColor: '#f5f5f5',
  },
  tableCell: {
    flex: 1,
    padding: 10,
    textAlign: 'center',
  },
  tableCell2: {
    flex: 1,
    padding: 10,
    textAlign: 'center',
    width: '30%',
  },
  headerCell: {
    fontWeight: 'bold',
  },
  prayerTitle: {
    fontWeight: 'bold',
    textAlign: 'left',
    paddingLeft: 10,
    fontSize: 18,
    color: '#fff',
  },
  prayerSalat: {
    fontWeight: 'bold',
    fontSize: 18,
    // textAlign: 'left',
    // paddingLeft: 10,
    color: '#fff',
  },
  prayerTitle2: {
    fontWeight: 'bold',
    textAlign: 'left',
    paddingLeft: 10,
  },
});
export default Donation;
