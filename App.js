import React, {useMemo, useState} from 'react';
// import Geolocation from '@react-native-community/geolocation';
import {StatusBar, StyleSheet} from 'react-native';
import 'react-native-gesture-handler';
import {GlobalContext} from './src/context/GlobalContext';
import MasterRoute from './src/navigation/MasterRoute';

const App = () => {
  const [globalState, setGlobalState] = useState({
    timing: null,
  });
  console.log('globalState App', globalState);

  const value = useMemo(() => {
    return {globalState, setGlobalState};
  }, [globalState, setGlobalState]);

  return (
    <GlobalContext.Provider value={value}>
      <StatusBar barStyle="light-content" backgroundColor={'#000'} />
      <MasterRoute />
    </GlobalContext.Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: StatusBar.currentHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  item: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default App;
